@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="post" action="{{route('users.update', ['user'=>$user])}}">
            @csrf
            <input name="_method" type="hidden" value="PUT">
            <div class="form-group">
                <label for="phone">Phone number</label>
                <input name="phone" class="form-control" id="phone" value="{{$user->phone}}">
                @error('phone') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label for="sex">Sex</label>
                <select name="sex" id="sex" class="form-control">
                    <option @if($user->sex === 'male') selected @endif value="male">Male</option>
                    <option @if($user->sex === 'female') selected @endif value="female">Female</option>
                </select>
                @error('sex') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label for="invite_type">Invite Type</label>
                <select name="invite_type" id="invite_type" class="form-control">
                    <option @if($user->invite_type === 'accept') selected @endif value="accept">Accept</option>
                    <option @if($user->invite_type === 'send') selected @endif value="send">Send</option>
                </select>
                @error('invite_type') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label for="sms_verified">Verified</label>
                <select name="sms_verified_at" id="sms_verified" class="form-control">
                    <option @if(!is_null($user->sms_verified_at)) selected @endif value="1">Yes</option>
                    <option @if(is_null($user->sms_verified_at)) selected @endif value="0">No</option>
                </select>
                @error('sms_verified_at') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="row">
                <button type="submit" class="btn btn-success">Submit</button>
                <a type="button" href="{{ route('users.index') }}" class="btn btn-secondary ml-2">Back</a>
            </div>
        </form>
    </div>
@endsection
