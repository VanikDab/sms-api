@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-9 offset-1">
                <table id="users" class="table table-striped table-bordered table-sm text-center" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm">Id</th>
                        <th class="th-sm">Phone</th>
                        <th class="th-sm">Sex</th>
                        <th class="th-sm">Invite</th>
                        <th class="th-sm">Edit </th>
                        <th class="th-sm">Delete</th>
                        <th class="th-sm">Accepted</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->sex }}</td>
                            <td>{{ $user->invite_type }}</td>
                            <td><a href="{{ route('users.edit', $user->id) }}" class="btn btn-info">Edit</a></td>
                            <td>
                                <a class="btn btn-danger" onclick="onDeleteConfirm(this)"><i class="fa fa-trash"></i>Delete</a>
                                <form action="{{route('users.destroy', $user->id)}}" method="post">
                                    <input type="hidden" name="_method" value="delete"/>
                                    @csrf
                                </form>
                            </td>
                            <td>{{$user->sms_verified_at ? 'YES' : 'NO'}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $users->links() }}
            </div>
        </div>
    </div>

@endsection
