<?php

namespace App\Http\Controllers\API;

use App\Classes\SMSRU;
use App\Model\SmsVerification;
use App\Repositories\Interfaces\UserApiRegistrationRepositoryInterface;
use App\Repositories\UserApiRegistrationRegistration;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RegisterController
{
    const CODE = 1111;

    private $apiRepository;

    public function __construct(UserApiRegistrationRegistration $userApi)
    {
        $this->apiRepository = $userApi;
    }

    public function register(Request $request)
    {
        $smsru = new SMSRU(config('app.sms_ru_key'));
        $data = new \stdClass();
        $data->to = request('phone');

        //$code = $this->generateCode();
        $code = self::CODE;

        $user = $this->storeUser();
        $result = $this->storeVerificationCode($user, $code);

        $data->text = 'Your SMS verification code is: ' . $code;
//        $sms = $smsru->send_one($data);
//
//        if ($sms->status == "OK") {
//            return response()->json(['data' => $result], Response::HTTP_OK);
//
//        } else {
//            //return "Сообщение не отправлено. ";
//
//            /*echo "Код ошибки: $sms->status_code. ";*/
//            return "Текст ошибки: $sms->status_text.";
//        }

        return response()->json(['data' => $result], Response::HTTP_OK);

    }

    private function storeUser()
    {
        return $this->apiRepository->storeUser();
    }

    public function storeVerificationCode($user, $code)
    {
        return $this->apiRepository->storeVerificationCode($user, $code);
    }

    public function validateSmsCode()
    {
        return $this->apiRepository->validateSmsCode();
    }

    private function generateCode()
    {
        $code = mt_rand(111111, 999999);
        return $code;
    }


}
