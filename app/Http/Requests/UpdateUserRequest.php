<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|max:20|unique:users,phone,'.request('user'),
            'sex' => 'required|in:male,female',
            'invite_type' => 'required|in:send,accept',
            'sms_verified_at' => 'in:0,1'
        ];
    }
}
