<?php

namespace App\Repositories;

use App\Model\SmsVerification;
use App\Repositories\Interfaces\UserApiRegistrationRepositoryInterface;
use App\User;

/**
 * Class UserRepository.
 */
class UserApiRegistrationRegistration implements UserApiRegistrationRepositoryInterface
{

    public function storeUser()
    {
        return User::updateOrCreate(
            ['phone' => request('phone')],
            [
                'invite_type' => request('invite_type'),
                'phone' => request('phone'),
                'sex' => request('sex'),
            ]);

    }

    public function storeVerificationCode($user, $code)
    {
        SmsVerification::updateOrCreate(
            ['user_id' => $user->id],
            [
                'user_id' => $user->id,
                'validation' => json_encode(['code' => $code, 'verified' => false])
            ]
        );

        $token = $user->createToken('token-name');

        return ['success' => true, 'token' => $token->plainTextToken];

    }


    public function validateSmsCode()
    {
        $user = request()->user();
        $smsValidation = $user->smsValidation;
        if ($smsValidation->first()) {
            $validation = $smsValidation->first()->validation;
            $validationObject = json_decode($validation);
            if (request('code') == $validationObject->code) {
                $validationObject->verified = true;
                $smsValidation->update(['validation' => json_encode($validationObject)]);

                return ['success' => true, 'message' => 'Code is valid'];
            }

            return ['success' => false, 'message' => 'Code is invalid'];
        }

        return ['success' => false, 'message' => 'Something went wrong'];
    }

}
