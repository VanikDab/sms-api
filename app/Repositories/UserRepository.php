<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserRepositoryInterface;
use App\User;
use Carbon\Carbon;

/**
 * Class UserRepository.
 */
class UserRepository implements UserRepositoryInterface
{
    const PAGINATE_COUNT = 10;

    public function all()
    {
        return User::where('id', '!=', auth()->id())->paginate(self::PAGINATE_COUNT);
    }

    public function edit($id)
    {
        return User::find($id);
    }

    public function update($request, $id)
    {
        $smsVerified = ($request->sms_verified_at == 1) ? Carbon::now() : NULL;

        $request->merge(['sms_verified_at' => $smsVerified]);

        User::find($id)->update($request->all());

        return true;
    }

    public function destroy($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();
        }

        return true;
    }


}
