<?php
namespace App\Repositories\Interfaces;

interface UserApiRegistrationRepositoryInterface
{
    public function storeUser();
    public function storeVerificationCode($user, $code);
    public function validateSmsCode();
}
