<?php
namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class  SmsVerification extends Model
{
    protected $table = 'sms_verifications';

    public $fillable = ['user_id', 'validation'];

    /**
     * Get the phone record associated with the user.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
